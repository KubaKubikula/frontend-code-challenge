import { gql } from "@apollo/client";

export const POKEMONS = gql`
  query (
    $limit: Int!
    $offset: Int!
    $search: String!
    $isFavorite: Boolean!
    $type: String!
  ) {
    pokemons(
      query: {
        limit: $limit
        offset: $offset
        search: $search
        filter: { isFavorite: $isFavorite, type: $type }
      }
    ) {
      count
      limit
      offset
      edges {
        id
        name
        image
        resistant
        isFavorite
      }
    }
  }
`;

export const POKEMON_BY_NAME = gql`
  query ($name: String!) {
    pokemonByName(name: $name) {
      id
      name
      image
      resistant
      maxCP
      maxHP
      weight {
        minimum
        maximum
      }
      height {
        minimum
        maximum
      }
      evolutions {
        name
        image
        id
        isFavorite
      }
      sound
      isFavorite
    }
  }
`;

export const POKEMON_TYPES = gql`
  query {
    pokemonTypes
  }
`;

export const ADD_FAVOURITE = gql`
  mutation ($id: ID!) {
    favoritePokemon(id: $id) {
      id
    }
  }
`;

export const UN_FAVOURITE = gql`
  mutation ($id: ID!) {
    unFavoritePokemon(id: $id) {
      id
    }
  }
`;
