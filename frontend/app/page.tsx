import List from "./components/List";
import Filter from "./components/Filter/Filter";
import type { Metadata } from "next";

export const metadata: Metadata = {
  title: "Pokedex :: Pokemon search ",
  description: "frontend test challenge",
};

export default function Home() {
  return (
    <main className="flex flex-col items-center justify-between pb-60">
      <div className="w-full max-w-[250px] sm:max-w-[466px] md:max-w-[698px] lg:max-w-[934px]">
        <Filter />
        <List />
      </div>
    </main>
  );
}
