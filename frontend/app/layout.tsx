import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import { ApolloWrapper } from "./ApolloWrapper";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Progress from "./components/Progress";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Pokemons",
  description: "Pokemons testing project",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={inter.className + " bg-amber-50/75"}>
        <ApolloWrapper>{children}</ApolloWrapper>
        <ToastContainer />
        <Progress />
      </body>
    </html>
  );
}
