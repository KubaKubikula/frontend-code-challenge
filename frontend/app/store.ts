import { create } from "zustand";

export const useFilterStore = create<FilterStore>((set) => ({
  listMode: "grid",
  isFavorites: false,
  search: "",
  type: "",
  offset: 0,
  setListMode: (listMode: "grid" | "list") =>
    set(() => ({ listMode: listMode })),
  setIsFavorites: (isFavorites: boolean) =>
    set(() => ({ isFavorites: isFavorites })),
  setSearch: (search: string) => set(() => ({ search: search })),
  setType: (type: string) => set(() => ({ type: type })),
  setOffset: (offset: number) => set(() => ({ offset: offset })),
}));

export type FilterStore = {
  listMode: "grid" | "list";
  isFavorites: boolean;
  search: string;
  type: string;
  offset: number;
  setListMode: (listMode: "grid" | "list") => void;
  setIsFavorites: (isFavorites: boolean) => void;
  setSearch: (search: string) => void;
  setType: (type: string) => void;
  setOffset: (offset: number) => void;
};
