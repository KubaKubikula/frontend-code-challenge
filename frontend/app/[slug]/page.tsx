import React from "react";
import CardDetail from "../components/CardDetail";
import type { Metadata, ResolvingMetadata } from "next";

const Detail = ({ params }: { params: { slug: string } }) => {
  return (
    <main className="flex flex-col items-center justify-between pt-3 bg-cyan-100/25 min-h-screen">
      <CardDetail name={params.slug} />
    </main>
  );
};

export async function generateMetadata(
  { params }: any,
  parent: ResolvingMetadata
): Promise<Metadata> {
  return {
    title: params.slug + " :: Pokedex",
  };
}

export default Detail;
