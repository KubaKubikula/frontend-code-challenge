"use client";

import React from "react";
import { AppProgressBar as ProgressBar } from "next-nprogress-bar";

const Progress = () => {
  return (
    <ProgressBar
      height="4px"
      color="#16a34a"
      options={{ showSpinner: false }}
      shallowRouting
    />
  );
};

export default Progress;
