import React from "react";
import { Select, Option } from "@material-tailwind/react";
import { useQuery } from "@apollo/experimental-nextjs-app-support/ssr";
import { useFilterStore } from "../../store";
import { POKEMON_TYPES } from "../../queries";
import { TiDeleteOutline } from "react-icons/ti";

const TypeField = () => {
  const { data, loading } = useQuery(POKEMON_TYPES);
  const { setType, type } = useFilterStore();

  return (
    <div>
      {!loading && data.pokemonTypes ? (
        <div className="relative">
          <Select
            variant="outlined"
            label="Select Type"
            className="bg-white"
            onChange={(value: string | undefined) =>
              value && value !== "" && setType(value)
            }
            value={type}
            placeholder="Pokemon type"
          >
            {data.pokemonTypes.map((type: string) => (
              <Option id={type} key={type} value={type}>
                {type}
              </Option>
            ))}
          </Select>
          {type && (
            <TiDeleteOutline
              onClick={() => setType("")}
              className="absolute top-[13px] right-8 cursor-pointer"
            />
          )}
        </div>
      ) : (
        <div className="w-[200px] h-[40px] animate-pulse bg-gray-300 rounded-lg"></div>
      )}
    </div>
  );
};

export default TypeField;
