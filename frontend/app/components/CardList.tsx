import React from "react";
import Image from "next/image";
import Link from "next/link";
import Heart from "./Heart";

const CardList = ({
  title,
  image,
  attributes,
  id,
  defaultIsFavourite,
}: {
  title: string;
  image: string;
  attributes: string[];
  id: number;
  defaultIsFavourite: boolean;
}) => {
  return (
    <Link href={`/${title.toLowerCase()}`}>
      <section className="cursor-pointer border border-gray-300 p-3 m-1 mb-0 rounded-lg shadow-sm hover:shadow-lg hover:scale-110 transition bg-white flex justify-between group">
        <aside className="flex flex-row">
          <Image
            src={image}
            width={100}
            height={100}
            alt={title}
            blurDataURL="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mOcVQMAAbYBGPlbdsUAAAAASUVORK5CYII="
          />
          <div className="pl-10">
            <div className="text-lg">{title}</div>
            <div className="flex flex-wrap text-sm my-2">
              {attributes.map((attrib: string) => (
                <div key={attrib} className="mr-1 ">
                  {attrib}
                </div>
              ))}
            </div>
          </div>
        </aside>
        <aside className="flex flex-col text-xl justify-center">
          <Heart
            pokemonId={id}
            className="text-red-500 hover:text-red-900 group-hover:animate-bounce"
            defaultIsFavourite={defaultIsFavourite}
          />
        </aside>
      </section>
    </Link>
  );
};

export default CardList;
