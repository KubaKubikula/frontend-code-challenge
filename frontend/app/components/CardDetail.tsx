"use client";

import React from "react";
import { useQuery } from "@apollo/experimental-nextjs-app-support/ssr";
import Image from "next/image";
import { TfiReload } from "react-icons/tfi";
import Card from "./CardGrid";
import { Progress } from "@material-tailwind/react";
import Heart from "./Heart";
import { POKEMON_BY_NAME } from "../queries";
import { Pokemon } from "../types";
import CardDetailNotExist from "./CardDetailNotExist";
import Speaker from "./Speaker";

const CardDetail = ({ name }: { name: string }) => {
  const { data, loading } = useQuery(POKEMON_BY_NAME, {
    variables: { name: name },
    fetchPolicy: "cache-and-network",
  });

  if (loading) {
    return (
      <div className="p-10">
        <TfiReload className="text-3xl animate-spin w-full flex flex-col justify-center text-center content-center text-gray-400" />
      </div>
    );
  }

  if (data.pokemonByName === null) {
    return <CardDetailNotExist />;
  }

  return (
    <section>
      <div className="border border-gray-300 rounded-lg p-4 m-1 group bg-white">
        <div className="text-2xl">{data.pokemonByName.name}</div>
        <div className="relative">
          <Image
            src={data.pokemonByName.image}
            width={350}
            height={350}
            alt={data.pokemonByName.name}
            className="m-8"
            blurDataURL="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mOcVQMAAbYBGPlbdsUAAAAASUVORK5CYII="
            priority
          />
          <Speaker sound={data.pokemonByName.sound} />
        </div>
        <div className="flex flex-wrap text-sm my-2">
          {data.pokemonByName.resistant.map((attrib: string) => (
            <div key={attrib} className="mr-1 ">
              {attrib}
            </div>
          ))}
        </div>
        <div className="flex flex-col gap-4">
          <div className="flex flex-row">
            <Progress
              placeholder="progress"
              value={(data.pokemonByName.maxHP * 100) / 5000}
              color="blue"
              className="mt-2 mr-2"
            />
            <div className="w-40">maxHP: {data.pokemonByName.maxHP}</div>
          </div>
          <div className="flex flex-row">
            <Progress
              placeholder="progress"
              value={(data.pokemonByName.maxCP * 100) / 5000}
              color="green"
              className="mt-2 mr-2"
            />
            <div className="w-40">maxCP: {data.pokemonByName.maxCP}</div>
          </div>

          <div className="flex flex-row text-center mb-10">
            <div className="w-1/2">
              Weight:{" "}
              <div className="pt-2">
                {data.pokemonByName.weight.minimum} -{" "}
                {data.pokemonByName.weight.maximum}
              </div>
            </div>
            <div className="w-1/2">
              Height:{" "}
              <div className="pt-2">
                {data.pokemonByName.height.minimum} -{" "}
                {data.pokemonByName.height.maximum}
              </div>
            </div>
          </div>
        </div>
        <div className="flex text-lg justify-center cursor-pointer ">
          <Heart
            pokemonId={data.pokemonByName.id}
            className="text-red-500 text-4xl group-hover:animate-bounce"
            defaultIsFavourite={data.pokemonByName.isFavorite}
          />
        </div>
      </div>
      <div className="flex flex-wrap pb-20 ">
        {data?.pokemonByName?.evolutions?.map((pokemon: Pokemon) => (
          <Card
            key={pokemon.id}
            id={pokemon.id}
            title={pokemon.name}
            image={pokemon.image}
            attributes={[]}
            defaultIsFavourite={pokemon.isFavorite}
          />
        ))}
      </div>
    </section>
  );
};

export default CardDetail;
