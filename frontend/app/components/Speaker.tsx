import React from "react";
import { PiSpeakerHighBold } from "react-icons/pi";

const Speaker = ({ sound }: { sound: string }) => {
  const audio = new Audio(sound);
  return (
    <PiSpeakerHighBold
      size={30}
      className="cursor-pointer hover:scale-110 absolute bottom-0 left-0"
      onClick={() => audio.play()}
    />
  );
};

export default Speaker;
