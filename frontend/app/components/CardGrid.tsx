import React from "react";
import Image from "next/image";
import Link from "next/link";
import Heart from "./Heart";

const CardGrid = ({
  title,
  image,
  attributes,
  id,
  defaultIsFavourite,
}: {
  title: string;
  image: string;
  attributes: string[];
  id: number;
  defaultIsFavourite: boolean;
}) => {
  return (
    <section className="w-56 cursor-pointer border border-gray-300 p-3 m-1 mb-0 rounded-lg shadow-sm hover:shadow-lg hover:scale-150 transition bg-white flex flex-col justify-between group">
      <Link href={`/${title.toLowerCase()}`}>
        <header>
          <div className="text-lg">{title}</div>
          <div className="my-10">
            <Image
              src={image}
              width={200}
              height={200}
              alt={title}
              blurDataURL="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mOcVQMAAbYBGPlbdsUAAAAASUVORK5CYII="
            />
          </div>
        </header>
        <div className="flex flex-wrap text-sm my-3">
          {attributes.map((attrib: string) => (
            <div key={attrib} className="mr-1 ">
              {attrib}
            </div>
          ))}
        </div>
      </Link>
      <footer className="flex text-2xl justify-center">
        <Heart
          pokemonId={id}
          className="text-red-500 hover:text-red-900 group-hover:animate-bounce"
          defaultIsFavourite={defaultIsFavourite}
        />
      </footer>
    </section>
  );
};

export default CardGrid;
