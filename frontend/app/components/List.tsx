"use client";

import React from "react";
import { useQuery } from "@apollo/experimental-nextjs-app-support/ssr";
import CardGrid from "./CardGrid";
import CardList from "./CardList";
import { TfiReload } from "react-icons/tfi";
import { useFilterStore } from "../store";
import { POKEMONS } from "../queries";
import { Pokemon } from "../types";
import { Waypoint } from "react-waypoint";
import Image from "next/image";

const List = () => {
  const limit = 16;
  const { listMode, isFavorites, search, type, offset, setOffset } =
    useFilterStore();
  const { data, loading, fetchMore } = useQuery(POKEMONS, {
    variables: {
      limit: limit,
      offset: 0,
      search: search,
      isFavorite: isFavorites,
      type,
    },
    fetchPolicy: isFavorites || search ? "no-cache" : "network-only",
  });

  const loadMore = () => {
    fetchMore({
      variables: {
        limit: limit,
        offset: offset + limit,
        search: search,
        isFavorite: isFavorites,
        type,
      },
    });
    setOffset(offset + limit);
  };

  if (loading) {
    return (
      <div className="p-10">
        <TfiReload className="text-3xl animate-spin w-full flex flex-col justify-center text-center content-center text-gray-400" />
      </div>
    );
  }

  return (
    <div>
      <div className={listMode === "list" ? "flex flex-col" : "flex flex-wrap"}>
        {data?.pokemons?.edges.map((pokemon: Pokemon) => (
          <>
            {listMode === "grid" ? (
              <CardGrid
                key={pokemon.id}
                id={pokemon.id}
                title={pokemon.name}
                image={pokemon.image}
                attributes={pokemon.resistant}
                defaultIsFavourite={pokemon.isFavorite}
              />
            ) : (
              <CardList
                key={pokemon.name}
                id={pokemon.id}
                title={pokemon.name}
                image={pokemon.image}
                attributes={pokemon.resistant}
                defaultIsFavourite={pokemon.isFavorite}
              />
            )}
          </>
        ))}

        {data?.pokemons?.edges?.length === 0 && (
          <div className="w-full border flex flex-col items-center p-20 text-gray-700 sepia">
            <header>Hasn&apos;t found any pokemon</header>
            <footer>
              <Image
                src="https://img.pokemondb.net/artwork/pikachu.jpg"
                width={200}
                height={200}
                alt={"Pokemon doesn&apos;t exist"}
                className="m-10 "
                blurDataURL="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mOcVQMAAbYBGPlbdsUAAAAASUVORK5CYII="
              />
            </footer>
          </div>
        )}
      </div>
      {data.pokemons.count > data.pokemons.limit + data.pokemons.offset && (
        <div className="p-10 flex flex-col items-center">
          <Waypoint onEnter={() => loadMore()} />
        </div>
      )}
    </div>
  );
};

export default List;
