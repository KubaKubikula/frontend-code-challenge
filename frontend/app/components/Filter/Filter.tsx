"use client";

import React from "react";
import {
  Input,
  IconButton,
  List,
  ListItem,
  ListItemPrefix,
  Radio,
  Typography,
} from "@material-tailwind/react";
import { useFilterStore } from "../../store";
import { FaList } from "react-icons/fa6";
import { IoGrid } from "react-icons/io5";
import TypeField from "./TypeField";

const Filter = () => {
  const {
    setListMode,
    listMode,
    setIsFavorites,
    isFavorites,
    search,
    setSearch,
  } = useFilterStore();

  return (
    <search className="w-full flex flex-col py-4 px-1 gap-1.5">
      <div className="w-full max-w-[24rem]">
        <List placeholder="Favourites" className="flex-row">
          <ListItem placeholder="Favourites" className="p-0" key="unfavourite">
            <label
              htmlFor="horizontal-list-react"
              className="flex w-full cursor-pointer items-center px-3 py-2"
            >
              <ListItemPrefix placeholder="Favourites" className="mr-3">
                <Radio
                  name="horizontal-list"
                  id="horizontal-list-react"
                  ripple={false}
                  className="hover:before:opacity-0"
                  containerProps={{
                    className: "p-0 bg-white",
                  }}
                  checked={!isFavorites}
                  crossOrigin="use-credentials"
                  onChange={() => setIsFavorites(false)}
                />
              </ListItemPrefix>
              <Typography
                color="blue-gray"
                className="font-medium text-blue-gray-400"
                placeholder="Favourites"
              >
                All
              </Typography>
            </label>
          </ListItem>
          <ListItem placeholder="Favourites" className="p-0" key="addfavourite">
            <label
              htmlFor="horizontal-list-vue"
              className="flex w-full cursor-pointer items-center px-3 py-2"
            >
              <ListItemPrefix placeholder="Favourites" className="mr-3">
                <Radio
                  name="horizontal-list"
                  id="horizontal-list-vue"
                  ripple={false}
                  className="hover:before:opacity-0"
                  containerProps={{
                    className: "p-0 bg-white",
                  }}
                  checked={isFavorites}
                  crossOrigin="use-credentials"
                  onChange={() => setIsFavorites(true)}
                />
              </ListItemPrefix>
              <Typography
                color="blue-gray"
                className="font-medium text-blue-gray-400"
                placeholder="Favourites"
              >
                Favourites
              </Typography>
            </label>
          </ListItem>
        </List>
      </div>
      <div className="flex flex-row flex-wrap gap-1.5">
        <div className="w-full md:w-[388px] lg:w-[620px]">
          <Input
            label="Search"
            crossOrigin="use-credentials"
            className="bg-white"
            value={search}
            onChange={(e) => setSearch(e.target.value)}
          />
        </div>
        <div className="flex flex-row gap-1.5">
          <TypeField />
          <IconButton
            onClick={() => setListMode("grid")}
            placeholder={"grid"}
            variant={listMode === "list" ? "outlined" : "filled"}
          >
            <IoGrid />
          </IconButton>
          <IconButton
            onClick={() => setListMode("list")}
            placeholder={"list"}
            variant={listMode === "grid" ? "outlined" : "filled"}
          >
            <FaList />
          </IconButton>
        </div>
      </div>
    </search>
  );
};

export default Filter;
