import React, { useState } from "react";
import { IoIosHeartEmpty, IoIosHeart } from "react-icons/io";
import { useMutation } from "@apollo/client";
import { ADD_FAVOURITE, UN_FAVOURITE } from "../queries";
import { toast } from "react-toastify";

const Heart = ({
  pokemonId,
  defaultIsFavourite,
  className = "",
}: {
  pokemonId: number;
  defaultIsFavourite: boolean;
  className: string;
}) => {
  const [isFavourite, setIsFavourite] = useState<boolean>(defaultIsFavourite);
  const [addFavourite] = useMutation(ADD_FAVOURITE);
  const [unFavourite] = useMutation(UN_FAVOURITE);
  const notify = (message: string) => toast(message);

  return (
    <span className={className}>
      {isFavourite ? (
        <IoIosHeart
          onClick={(event: React.ChangeEvent<HTMLInputElement>) => {
            unFavourite({ variables: { id: pokemonId } });
            setIsFavourite(false);
            notify("Removed to favourites!");
            event.preventDefault();
          }}
        />
      ) : (
        <IoIosHeartEmpty
          onClick={(event: React.ChangeEvent<HTMLInputElement>) => {
            addFavourite({ variables: { id: pokemonId } });
            setIsFavourite(true);
            notify("Added to favourites!");
            event.preventDefault();
          }}
        />
      )}
    </span>
  );
};

export default Heart;
