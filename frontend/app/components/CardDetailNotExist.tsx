import React from "react";

import Image from "next/image";
import { Progress } from "@material-tailwind/react";

const CardDetailNotExist = () => {
  return (
    <section className="border border-gray-300 rounded-lg p-4 max-w-screen-lg m-1 group bg-white sepia">
      <header className="text-2xl">Pokemon doesn't exist</header>
      <div className="relative">
        <Image
          src="https://img.pokemondb.net/artwork/pikachu.jpg"
          width={500}
          height={500}
          alt={"Pokemon doesn&apos;t exist"}
          className="m-10 "
          blurDataURL="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mOcVQMAAbYBGPlbdsUAAAAASUVORK5CYII="
        />
      </div>
      <div className="flex w-full flex-col gap-4">
        <div className="flex flex-row">
          <Progress
            placeholder="progress"
            value={(1000 * 100) / 5000}
            color="blue"
            className="mt-2 mr-2"
          />
          <div className="w-40">maxHP: N/A</div>
        </div>
        <div className="flex flex-row">
          <Progress
            placeholder="progress"
            value={(1000 * 100) / 5000}
            color="green"
            className="mt-2 mr-2"
          />
          <div className="w-40">maxCP: N/A</div>
        </div>

        <footer className="flex flex-row text-center mb-10">
          <div className="w-1/2">
            Weight: <div className="pt-2">N/A</div>
          </div>
          <div className="w-1/2">
            Height: <div className="pt-2">N/A</div>
          </div>
        </footer>
      </div>
    </section>
  );
};

export default CardDetailNotExist;
